#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

class Laboratorio:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_lab = constructor.get_object("vent_visualizar")
        self.ventana_lab.show_all()

        self.btn_agregar = constructor.get_object("añadir")
        self.btn_agregar.connect("clicked", self.agregar_lab)

        self.btn_editar = constructor.get_object("editar")
        self.btn_editar.connect("clicked", self.editar_lab)

        self.btn_borrar = constructor.get_object("eliminar")
        self.btn_borrar.connect("clicked", self.eliminar_lab)

        self.btn_cerrar = constructor.get_object("cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.treeview_lab = constructor.get_object("lista")

        self.set_columnas_lab()

        self.get_lab()

    def set_columnas_lab(self):
        for col in self.treeview_lab.get_columns():
            self.treeview_lab.remove_column(col)
        
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Código", renderer, text=0)
        self.treeview_lab.append_column(column)


        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_lab.append_column(column)

        self.store = Gtk.ListStore(int, str) 
        self.treeview_lab.set_model(self.store)

    def get_lab(self):
        db = Database()

        sql = """
            select idlaboratorio, nombre from laboratorio
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1]])


    def destruir_ventana(self, *args):
        self.ventana_lab.hide()

    def agregar_lab(self, button):
        agregar = Nuevo_laboratorio(True, self);

    def editar_lab(self, button):
        editar = Nuevo_laboratorio(False, self);

    def eliminar_lab(self, button):
        seleccion = self.treeview_lab.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_lab, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar(model[treeiter][0])
                self.get_lab()

            dialog.destroy()

    def eliminar(self, id_lab):
        db = Database()
        sql = """
            delete from laboratorio where idlaboratorio = %(id_lab)s
            """
        db.run_sql(sql, {'id_lab': id_lab}, 'E')


class Nuevo_laboratorio:
    def __init__(self, agregar_laboratorio, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_agregar")
        self.ventana_agregar.set_title("Registro laboratorio")

        self.btn_cancelar = constructor.get_object("cancelar_agregar")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.btn_guardar = constructor.get_object("guardar_agregar")

        self.nombre_lab = constructor.get_object("nombre_agregar")

        if agregar_laboratorio:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_lab)

        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_lab)

            seleccion = self.obj_padre.treeview_lab.get_selection()
            model, treeiter = seleccion.get_selected()
    
            if treeiter is not None:
                self.id_lab = model[treeiter][0]
                db = Database()
                
                sql = """
                    select nombre from laboratorio where idlaboratorio = %(id_lab)s
                    """
                
                result = db.run_select_filter(sql, {'id_lab': self.id_lab})
                self.nombre_lab.set_text(str(result[0][0]))
                
                self.ventana_agregar.show_all()

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def guardar_lab(self, button):
        nombre = self.nombre_lab.get_text()
        db = Database()
        
        if nombre != "": 
            sql = """
                insert into laboratorio (nombre) values (%(nom_lab)s)
                """
            db.run_sql(sql, {"nom_lab": nombre}, 'I')
        
            self.obj_padre.get_lab()
            self.ventana_agregar.hide()
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()

    def actualizar_lab(self, button):
        nombre = self.nombre_lab.get_text()
        db = Database()
        
        if nombre != "":
            sql = """
                update laboratorio set nombre = %(nom_lab)s where idlaboratorio = %(id_lab)s
                """
            db.run_sql(sql, {"id_lab": self.id_lab, "nom_lab": nombre}, 'A')
        
            self.obj_padre.get_lab()
            self.ventana_agregar.hide()   
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()
