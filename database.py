#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import mysql.connector
from utils import Mensage

"""
https://dev.mysql.com/doc/connector-python/en/connector-python-examples.html
"""
class Database:
    def __init__(self):
        self.db = None
        self.cursor = None
        
        try:
            self.db = mysql.connector.connect(host="localhost", 
                user="root", passwd="nicole", database="db_vet")
            self.cursor = self.db.cursor()
        except mysql.connector.Error as err:
            msg = Mensage(Gtk.MessageType.ERROR, err)
   
    # Permite traer datos de la base de datos 
    def run_select(self, sql):
        result = None
    
        try:
            self.cursor.execute(sql)
            result = self.cursor.fetchall()
        except mysql.connector.Error as err:
            print(err)
            msg = Mensage(Gtk.MessageType.ERROR, "Error: No es posible visualizar los datos")
    
        return result

    def run_select_filter(self, sql, params):
        result = None
    
        try:
            self.cursor.execute(sql, params)
            result = self.cursor.fetchall()
        except mysql.connector.Error as err:
            print(err)
            msg = Mensage(Gtk.MessageType.ERROR, err)
    
        return result

    # Realiza operaciones (inserción, actualización y eliminación)
    def run_sql(self, sql, params, accion):    
        try:
            self.cursor.execute(sql, params)
            self.db.commit()
        except mysql.connector.Error as err:
            print(err)
            self.db.rollback()
            if accion == 'I':
                msg = Mensage(Gtk.MessageType.ERROR, "Error: No se puede realizar la inserción")

            elif accion == 'A':
                msg = Mensage(Gtk.MessageType.ERROR, "Error: No se puede realizar la actualización")

            elif accion == 'E':
                msg = Mensage(Gtk.MessageType.ERROR, "Error: No se puede realizar la eliminación")
    
            
