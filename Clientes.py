#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database
from datetime import date
from datetime import datetime


def valor(result):
    for i in result:
        for j in i:
            codigo = j
    return codigo


class Clientes:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_cli = constructor.get_object("vent_cliente")
        self.ventana_cli.show_all()

        self.btn_cerrar = constructor.get_object("cerrar_info")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.entrada = constructor.get_object("entrada_cliente")

        self.treeview_estado = constructor.get_object("lista_estado")
        self.set_columnas_estado()

        self.treeview_info = constructor.get_object("lista_atencion")
        self.set_columnas_info()

        self.treeview_receta = constructor.get_object("lista_receta")
        self.set_columnas_receta()

        self.btn_buscar = constructor.get_object("buscar_masc")
        self.btn_buscar.connect("clicked", self.get_estado)
        self.btn_buscar.connect("clicked", self.get_info)
        self.btn_buscar.connect("clicked", self.get_receta)

    def destruir_ventana(self, *args):
        self.ventana_cli.hide()

    def set_columnas_info(self):
        for col in self.treeview_info.get_columns():
            self.treeview_info.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=0)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Chip", renderer, text=1)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Peso", renderer, text=2)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Raza", renderer, text=3)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha ingreso", renderer, text=4)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha retiro", renderer, text=5)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Veterinario", renderer, text=6)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Rut", renderer, text=7)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Teléfono", renderer, text=8)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Servicio", renderer, text=9)
        self.treeview_info.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Valor atención", renderer, text=10)
        self.treeview_info.append_column(column)

        self.store = Gtk.ListStore(str, bool, float, str, str, str, str, int, int, str, int) 
        self.treeview_info.set_model(self.store)

    def get_info(self, button):
        codigo = self.entrada.get_text()
        db = Database()

        sql = """
            select nom_masc, chip, peso, nom_raza, fecha_ingreso, fecha_retiro, nom_tra, trabajador_rut, 
            num_telefono, nom_ser, precio from view_registro where codigo_masc = %(codigo_mascota)s;
            """ 
        result = db.run_select_filter(sql, {"codigo_mascota": codigo})
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2], r[3], str(r[4]), str(r[5]), r[6], r[7], r[8], r[9], r[10]])

    def set_columnas_estado(self):
        for col in self.treeview_estado.get_columns():
            self.treeview_estado.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Código ficha", renderer, text=0)
        self.treeview_estado.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Paciente", renderer, text=1)
        self.treeview_estado.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha ingreso", renderer, text=2)
        self.treeview_estado.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Estado atención", renderer, text=3)
        self.treeview_estado.append_column(column)

        self.store_estado = Gtk.ListStore(str, str, str, str) 
        self.treeview_estado.set_model(self.store_estado)

    def get_estado(self, button):
        codigo = self.entrada.get_text()

        db = Database()

        tener_estado = """
            select estado_atencion from view_estado where mascota_codigo_masc = %(codigo_mascota)s;
            """ 
        result = db.run_select_filter(tener_estado, {"codigo_mascota": codigo})
        
        if result == []:
            dialog = Gtk.MessageDialog(parent=self.ventana_cli, flags=0, message_type=Gtk.MessageType.INFO, 
                buttons=Gtk.ButtonsType.OK, text="Alerta")
            dialog.format_secondary_text("Su mascota aún no ha sido ingresada a la sala de espera.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()

        else:
            sql = """
                select codigo_regi, nombre_masc, fecha_ingreso, estado_atencion from view_estado where mascota_codigo_masc = %(codigo_mascota)s;
                """ 
            result = db.run_select_filter(sql, {"codigo_mascota": codigo})
        
            self.store_estado.clear()
        
            if result:
                for r in result:
                    self.store_estado.append([str(r[0]), r[1], str(r[2]), r[3]])

    def set_columnas_receta(self):
        for col in self.treeview_receta.get_columns():
            self.treeview_receta.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Medicamento", renderer, text=0)
        self.treeview_receta.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Dosis", renderer, text=1)
        self.treeview_receta.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha ingreso", renderer, text=2)
        self.treeview_receta.append_column(column)

        self.store_receta = Gtk.ListStore(str, str, str) 
        self.treeview_receta.set_model(self.store_receta)

    def get_receta(self, button):
        codigo = self.entrada.get_text()
        db = Database()

        sql = """
            select nombre_med, dosis, fecha_ingreso from view_registro_med where codigo_masc = %(codigo_mascota)s;
            """ 
        result = db.run_select_filter(sql, {"codigo_mascota": codigo})
        
        self.store_receta.clear()
        
        if result:
            for r in result:
                self.store_receta.append([r[0], r[1], str(r[2])])
