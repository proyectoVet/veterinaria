#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

def valor_combo(combo, posicion):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][posicion]
    return seleccion

def editar_combo(codigo, combo, posicion):
    if codigo:
        index = 0
        for ite in combo.get_model():
            if (ite[posicion] == codigo):
                combo.set_active(index)
                break
            index += 1

    else:
        combo.set_active(0)


class Mascotas:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")

        self.ventana_mascota = constructor.get_object("vent_visualizar")
        self.ventana_mascota.show_all()

        self.btn_agregar = constructor.get_object("añadir")
        self.btn_agregar.connect("clicked", self.agregar_mascota)

        self.btn_editar = constructor.get_object("editar")
        self.btn_editar.connect("clicked", self.editar_mascota)

        self.btn_borrar = constructor.get_object("eliminar")
        self.btn_borrar.connect("clicked", self.eliminar_mascota)

        self.btn_cerrar = constructor.get_object("cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.treeview_mascotas = constructor.get_object("lista")

        self.set_columnas_mascotas()

        self.get_mascotas()

    def destruir_ventana(self, *args):
        self.ventana_mascota.hide()

    def set_columnas_mascotas(self):
        for col in self.treeview_mascotas.get_columns():
            self.treeview_mascotas.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Código", renderer, text=0)
        self.treeview_mascotas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_mascotas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Peso", renderer, text=2)
        self.treeview_mascotas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Chip", renderer, text=3)
        self.treeview_mascotas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Raza", renderer, text=4)
        self.treeview_mascotas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Dueño", renderer, text=5)
        self.treeview_mascotas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Rut", renderer, text=6)
        self.treeview_mascotas.append_column(column)

        self.store = Gtk.ListStore(int, str, float, bool, str, str, str) 
        self.treeview_mascotas.set_model(self.store)

    def get_mascotas(self):
        db = Database()

        sql = """
            select * from view_pacientes;
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2], r[3], r[4], r[5], str(r[6])])

    def agregar_mascota(self, button):
        agregar = Nueva_mascota(True, self);

    def editar_mascota(self, button):
        editar = Nueva_mascota(False, self);

    def eliminar_mascota(self, button):
        seleccion = self.treeview_mascotas.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_mascota, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar(model[treeiter][0])
                self.get_mascotas()

            dialog.destroy()

    def eliminar(self, id_mascota):
        db = Database()
        sql = """
            delete from mascota where codigo_masc = %(id_mascota)s
            """
        db.run_sql(sql, {'id_mascota': id_mascota}, 'E')

class Nueva_mascota:
    def __init__(self, agregar_mascota, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")

        self.obj_padre = objeto
        
        self.ventana_agregar = constructor.get_object("vent_masc")
        self.ventana_agregar.show_all()

        self.btn_guardar = constructor.get_object("boton_guardar_masc")

        self.btn_cancelar = constructor.get_object("boton_cancelar_masc")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.codigo_masc = constructor.get_object("codigo_masc")
        self.nombre_masc = constructor.get_object("nombre_masc")
        self.peso_masc = constructor.get_object("peso")

        self.combo_chip = constructor.get_object("chip")        

        self.combo_raza = constructor.get_object("raza")

        renderer = Gtk.CellRendererText()
        self.combo_raza.pack_start(renderer, False)
        self.combo_raza.add_attribute(renderer, "text", 0)
        
        self.store = Gtk.ListStore(str)
        self.combo_raza.set_model(self.store)
        self.get_raza()

        self.combo_cliente = constructor.get_object("combo_cliente")
        renderer = Gtk.CellRendererText()
        self.combo_cliente.pack_start(renderer, False)
        self.combo_cliente.add_attribute(renderer, "text", 0)

        renderer = Gtk.CellRendererText()
        self.combo_cliente.pack_start(renderer, False)
        self.combo_cliente.add_attribute(renderer, "text", 1)

        self.store = Gtk.ListStore(str, str)
        self.combo_cliente.set_model(self.store)
        self.get_cliente()

        if agregar_mascota:
            self.combo_cliente.set_active(0)
            self.combo_chip.set_active(0)
            self.combo_raza.set_active(0)

            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_mascota)

        else:
            self.ventana_agregar.show_all()

            self.btn_guardar.connect("clicked", self.actualizar_mascotas)

            seleccion = self.obj_padre.treeview_mascotas.get_selection()
            model, treeiter = seleccion.get_selected()

            if treeiter is not None:
                self.id_mascota = model[treeiter][0]
                db = Database()
                
                sql = """
                    select * from view_pacientes where codigo_masc = %(id_mascota)s;
                    """
                
                result = db.run_select_filter(sql, {'id_mascota': self.id_mascota})
                self.codigo_masc.set_text(str(result[0][0]))
                self.nombre_masc.set_text(str(result[0][1]))
                self.peso_masc.set_text(str(result[0][2]))
                
                chip = result[0][3]

                if chip == True:
                    chip = "Si"

                elif chip == False:
                    chip = "No"

                editar_combo(chip, self.combo_chip, 0)

                raza = result[0][4]
                editar_combo(raza, self.combo_raza, 0)

                cliente = result[0][5]
                editar_combo(cliente, self.combo_cliente, 0)

                self.ventana_agregar.show_all()

    def get_raza(self):
        db = Database()
        
        sql = """
            select nombre from raza
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append(["Seleccionar"])
            for r in result:
                self.store.append([r[0]])

    def get_cliente(self):
        db = Database()
        
        sql = """
            select nombre, rut from cliente
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append(["Seleccionar", " "])
            for r in result:
                self.store.append([r[0], str(r[1])])

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def guardar_mascota(self, button):
        codigo_mascota = self.codigo_masc.get_text()
        nombre = self.nombre_masc.get_text()
        peso = self.peso_masc.get_text()
        
        nombre_raza = valor_combo(self.combo_raza, 0)
        rut = valor_combo(self.combo_cliente, 1)
        chip = valor_combo(self.combo_chip, 0)
        
        if chip == "Si":
            valor_chip = True

        elif chip == "No":
            valor_chip = False

        db = Database()

        obtener_id = """
            select idraza from raza where nombre = %(nom_raza)s;
            """ 
        result = db.run_select_filter(obtener_id, {"nom_raza": nombre_raza})
        
        for i in result:
            for j in i:
                id_raza = j
    
        if codigo_mascota != " " and nombre != " " and peso != " " and nombre_raza and "Seleccionar" and rut != " " and chip != "Seleccionar":
            sql = """
                insert into mascota (codigo_masc, nombre, peso, chip, cliente_rut, raza_idraza) 
                values (%(codigo)s, %(nom)s, %(peso)s, %(chip)s, %(rut)s, %(raza)s)
                """
            db.run_sql(sql, {"codigo": codigo_mascota, "nom": nombre, "peso": peso, "chip": valor_chip, "rut": rut, "raza": id_raza}, 'I')
            self.obj_padre.get_mascotas()
            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()

    def actualizar_mascotas(self, button):
        codigo_mascota = self.codigo_masc.get_text()
        nombre = self.nombre_masc.get_text()
        peso = self.peso_masc.get_text()
        
        nombre_raza = valor_combo(self.combo_raza, 0)
        rut = valor_combo(self.combo_cliente, 1)
        chip = valor_combo(self.combo_chip, 0)
        
        if chip == "Si":
            valor_chip = True

        elif chip == "No":
            valor_chip = False
        
        db = Database()

        obtener_id = """
            select idraza from raza where nombre = %(nom_raza)s;
            """ 
        result = db.run_select_filter(obtener_id, {"nom_raza": nombre_raza})
        
        for i in result:
            for j in i:
                id_raza = j
        
        if codigo_mascota != " " and nombre != " " and peso != " " and nombre_raza and "Seleccionar" and rut != " " and chip != "Seleccionar":
            sql = """
                update mascota set codigo_masc = %(id)s, nombre = %(nom)s, peso = %(peso)s, chip = %(chip)s, 
                cliente_rut = %(rut)s, raza_idraza = %(raza)s where codigo_masc = %(id)s
                """
            db.run_sql(sql, {"id": codigo_mascota, "nom": nombre, "peso": peso, "chip": valor_chip, "rut": rut, "raza": id_raza}, 'A')
        
            self.obj_padre.get_mascotas()
            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()
