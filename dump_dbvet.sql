-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: db_vet
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `rut` int NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `num_telefono` int NOT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laboratorio`
--

DROP TABLE IF EXISTS `laboratorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `laboratorio` (
  `idlaboratorio` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idlaboratorio`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laboratorio`
--

LOCK TABLES `laboratorio` WRITE;
/*!40000 ALTER TABLE `laboratorio` DISABLE KEYS */;
/*!40000 ALTER TABLE `laboratorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mascota`
--

DROP TABLE IF EXISTS `mascota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mascota` (
  `codigo_masc` int NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `peso` float NOT NULL,
  `chip` tinyint(1) NOT NULL,
  `cliente_rut` int NOT NULL,
  `raza_idraza` int DEFAULT NULL,
  PRIMARY KEY (`codigo_masc`),
  KEY `fk_mascota_cliente_idx` (`cliente_rut`),
  KEY `fk_mascota_raza1_idx` (`raza_idraza`),
  CONSTRAINT `fk_mascota_cliente` FOREIGN KEY (`cliente_rut`) REFERENCES `cliente` (`rut`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_mascota_raza1` FOREIGN KEY (`raza_idraza`) REFERENCES `raza` (`idraza`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mascota`
--

LOCK TABLES `mascota` WRITE;
/*!40000 ALTER TABLE `mascota` DISABLE KEYS */;
/*!40000 ALTER TABLE `mascota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicamento`
--

DROP TABLE IF EXISTS `medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medicamento` (
  `codigo_med` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `laboratorio_idlaboratorio` int DEFAULT NULL,
  PRIMARY KEY (`codigo_med`),
  KEY `fk_medicamento_laboratorio1_idx` (`laboratorio_idlaboratorio`),
  CONSTRAINT `fk_medicamento_laboratorio1` FOREIGN KEY (`laboratorio_idlaboratorio`) REFERENCES `laboratorio` (`idlaboratorio`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicamento`
--

LOCK TABLES `medicamento` WRITE;
/*!40000 ALTER TABLE `medicamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raza`
--

DROP TABLE IF EXISTS `raza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `raza` (
  `idraza` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idraza`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raza`
--

LOCK TABLES `raza` WRITE;
/*!40000 ALTER TABLE `raza` DISABLE KEYS */;
/*!40000 ALTER TABLE `raza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_atencion`
--

DROP TABLE IF EXISTS `registro_atencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registro_atencion` (
  `codigo_regi` int NOT NULL AUTO_INCREMENT,
  `fecha_ingreso` date NOT NULL,
  `fecha_retiro` date NOT NULL,
  `mascota_codigo_masc` int NOT NULL,
  `estado_atencion` varchar(45) NOT NULL,
  PRIMARY KEY (`codigo_regi`),
  KEY `fk_registro atencion_mascota1_idx` (`mascota_codigo_masc`),
  CONSTRAINT `fk_registro atencion_mascota1` FOREIGN KEY (`mascota_codigo_masc`) REFERENCES `mascota` (`codigo_masc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_atencion`
--

LOCK TABLES `registro_atencion` WRITE;
/*!40000 ALTER TABLE `registro_atencion` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro_atencion` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trigger_sala_espera` BEFORE INSERT ON `registro_atencion` FOR EACH ROW BEGIN
      set new.estado_atencion = "En espera";
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `registro_has_medicamento`
--

DROP TABLE IF EXISTS `registro_has_medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registro_has_medicamento` (
  `registro_codigo_regi` int NOT NULL,
  `medicamento_codigo_med` int NOT NULL,
  `dosis` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`registro_codigo_regi`,`medicamento_codigo_med`),
  KEY `fk_registro_has_medicamento_medicamento1_idx` (`medicamento_codigo_med`),
  KEY `fk_registro_has_medicamento_registro1_idx` (`registro_codigo_regi`),
  CONSTRAINT `fk_registro_has_medicamento_medicamento1` FOREIGN KEY (`medicamento_codigo_med`) REFERENCES `medicamento` (`codigo_med`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_registro_has_medicamento_registro1` FOREIGN KEY (`registro_codigo_regi`) REFERENCES `registro_atencion` (`codigo_regi`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_has_medicamento`
--

LOCK TABLES `registro_has_medicamento` WRITE;
/*!40000 ALTER TABLE `registro_has_medicamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro_has_medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_has_servicio`
--

DROP TABLE IF EXISTS `registro_has_servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registro_has_servicio` (
  `registro_codigo_regi` int NOT NULL,
  `servicio_codigo_ser` int NOT NULL,
  `trabajador_rut` int NOT NULL,
  PRIMARY KEY (`registro_codigo_regi`,`servicio_codigo_ser`),
  KEY `fk_registro_has_servicio_servicio1_idx` (`servicio_codigo_ser`),
  KEY `fk_registro_has_servicio_registro1_idx` (`registro_codigo_regi`),
  KEY `fk_registro_has_servicio_trabajador1_idx` (`trabajador_rut`),
  CONSTRAINT `fk_registro_has_servicio_registro1` FOREIGN KEY (`registro_codigo_regi`) REFERENCES `registro_atencion` (`codigo_regi`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_registro_has_servicio_servicio1` FOREIGN KEY (`servicio_codigo_ser`) REFERENCES `servicio` (`codigo_ser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_registro_has_servicio_trabajador1` FOREIGN KEY (`trabajador_rut`) REFERENCES `trabajador` (`rut`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_has_servicio`
--

LOCK TABLES `registro_has_servicio` WRITE;
/*!40000 ALTER TABLE `registro_has_servicio` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro_has_servicio` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trigger_atencion` BEFORE INSERT ON `registro_has_servicio` FOR EACH ROW BEGIN 
update registro_atencion set estado_atencion = "Atendido" where codigo_regi = new.registro_codigo_regi;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trigger_eliminar_atencion` BEFORE DELETE ON `registro_has_servicio` FOR EACH ROW BEGIN
update registro_atencion set estado_atencion = "En espera" where codigo_regi = old.registro_codigo_regi; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `servicio`
--

DROP TABLE IF EXISTS `servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servicio` (
  `codigo_ser` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `precio` int DEFAULT NULL,
  PRIMARY KEY (`codigo_ser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio`
--

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabajador`
--

DROP TABLE IF EXISTS `trabajador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trabajador` (
  `rut` int NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `num_telefono` int NOT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabajador`
--

LOCK TABLES `trabajador` WRITE;
/*!40000 ALTER TABLE `trabajador` DISABLE KEYS */;
/*!40000 ALTER TABLE `trabajador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view_estado`
--

DROP TABLE IF EXISTS `view_estado`;
/*!50001 DROP VIEW IF EXISTS `view_estado`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_estado` AS SELECT 
 1 AS `codigo_regi`,
 1 AS `nombre_masc`,
 1 AS `fecha_ingreso`,
 1 AS `fecha_retiro`,
 1 AS `estado_atencion`,
 1 AS `mascota_codigo_masc`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_med_lab`
--

DROP TABLE IF EXISTS `view_med_lab`;
/*!50001 DROP VIEW IF EXISTS `view_med_lab`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_med_lab` AS SELECT 
 1 AS `codigo_med`,
 1 AS `nom_med`,
 1 AS `nom_lab`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_pacientes`
--

DROP TABLE IF EXISTS `view_pacientes`;
/*!50001 DROP VIEW IF EXISTS `view_pacientes`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_pacientes` AS SELECT 
 1 AS `codigo_masc`,
 1 AS `nombre`,
 1 AS `peso`,
 1 AS `chip`,
 1 AS `nombre_raza`,
 1 AS `nombre_cli`,
 1 AS `rut`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_registro`
--

DROP TABLE IF EXISTS `view_registro`;
/*!50001 DROP VIEW IF EXISTS `view_registro`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_registro` AS SELECT 
 1 AS `codigo_masc`,
 1 AS `nom_masc`,
 1 AS `peso`,
 1 AS `chip`,
 1 AS `nom_raza`,
 1 AS `fecha_ingreso`,
 1 AS `fecha_retiro`,
 1 AS `registro_codigo_regi`,
 1 AS `trabajador_rut`,
 1 AS `id_ser`,
 1 AS `nom_tra`,
 1 AS `rut`,
 1 AS `num_telefono`,
 1 AS `nom_ser`,
 1 AS `precio`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_registro_med`
--

DROP TABLE IF EXISTS `view_registro_med`;
/*!50001 DROP VIEW IF EXISTS `view_registro_med`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_registro_med` AS SELECT 
 1 AS `codigo_masc`,
 1 AS `nom_masc`,
 1 AS `peso`,
 1 AS `chip`,
 1 AS `nom_raza`,
 1 AS `fecha_ingreso`,
 1 AS `fecha_retiro`,
 1 AS `trabajador_rut`,
 1 AS `id_ser`,
 1 AS `nom_tra`,
 1 AS `rut`,
 1 AS `num_telefono`,
 1 AS `nom_ser`,
 1 AS `precio`,
 1 AS `codigo_regi`,
 1 AS `id_med`,
 1 AS `nombre_med`,
 1 AS `dosis`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_estado`
--

/*!50001 DROP VIEW IF EXISTS `view_estado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_estado` AS select `registro_atencion`.`codigo_regi` AS `codigo_regi`,`mascota`.`nombre` AS `nombre_masc`,`registro_atencion`.`fecha_ingreso` AS `fecha_ingreso`,`registro_atencion`.`fecha_retiro` AS `fecha_retiro`,`registro_atencion`.`estado_atencion` AS `estado_atencion`,`registro_atencion`.`mascota_codigo_masc` AS `mascota_codigo_masc` from (`registro_atencion` join `mascota` on((`mascota`.`codigo_masc` = `registro_atencion`.`mascota_codigo_masc`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_med_lab`
--

/*!50001 DROP VIEW IF EXISTS `view_med_lab`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_med_lab` AS select `medicamento`.`codigo_med` AS `codigo_med`,`medicamento`.`nombre` AS `nom_med`,`laboratorio`.`nombre` AS `nom_lab` from (`medicamento` join `laboratorio` on((`medicamento`.`laboratorio_idlaboratorio` = `laboratorio`.`idlaboratorio`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_pacientes`
--

/*!50001 DROP VIEW IF EXISTS `view_pacientes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_pacientes` AS select `mascota`.`codigo_masc` AS `codigo_masc`,`mascota`.`nombre` AS `nombre`,`mascota`.`peso` AS `peso`,`mascota`.`chip` AS `chip`,`raza`.`nombre` AS `nombre_raza`,`cliente`.`nombre` AS `nombre_cli`,`cliente`.`rut` AS `rut` from ((`mascota` join `cliente` on((`mascota`.`cliente_rut` = `cliente`.`rut`))) join `raza` on((`mascota`.`raza_idraza` = `raza`.`idraza`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_registro`
--

/*!50001 DROP VIEW IF EXISTS `view_registro`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_registro` AS select `mascota`.`codigo_masc` AS `codigo_masc`,`mascota`.`nombre` AS `nom_masc`,`mascota`.`peso` AS `peso`,`mascota`.`chip` AS `chip`,`raza`.`nombre` AS `nom_raza`,`registro_atencion`.`fecha_ingreso` AS `fecha_ingreso`,`registro_atencion`.`fecha_retiro` AS `fecha_retiro`,`registro_has_servicio`.`registro_codigo_regi` AS `registro_codigo_regi`,`registro_has_servicio`.`trabajador_rut` AS `trabajador_rut`,`registro_has_servicio`.`servicio_codigo_ser` AS `id_ser`,`trabajador`.`nombre` AS `nom_tra`,`trabajador`.`rut` AS `rut`,`trabajador`.`num_telefono` AS `num_telefono`,`servicio`.`nombre` AS `nom_ser`,`servicio`.`precio` AS `precio` from (((((`mascota` join `raza` on((`raza`.`idraza` = `mascota`.`raza_idraza`))) join `registro_atencion` on((`mascota`.`codigo_masc` = `registro_atencion`.`mascota_codigo_masc`))) join `registro_has_servicio` on((`registro_atencion`.`codigo_regi` = `registro_has_servicio`.`registro_codigo_regi`))) join `trabajador` on((`registro_has_servicio`.`trabajador_rut` = `trabajador`.`rut`))) join `servicio` on((`registro_has_servicio`.`servicio_codigo_ser` = `servicio`.`codigo_ser`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_registro_med`
--

/*!50001 DROP VIEW IF EXISTS `view_registro_med`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_registro_med` AS select `mascota`.`codigo_masc` AS `codigo_masc`,`mascota`.`nombre` AS `nom_masc`,`mascota`.`peso` AS `peso`,`mascota`.`chip` AS `chip`,`raza`.`nombre` AS `nom_raza`,`registro_atencion`.`fecha_ingreso` AS `fecha_ingreso`,`registro_atencion`.`fecha_retiro` AS `fecha_retiro`,`registro_has_servicio`.`trabajador_rut` AS `trabajador_rut`,`registro_has_servicio`.`servicio_codigo_ser` AS `id_ser`,`trabajador`.`nombre` AS `nom_tra`,`trabajador`.`rut` AS `rut`,`trabajador`.`num_telefono` AS `num_telefono`,`servicio`.`nombre` AS `nom_ser`,`servicio`.`precio` AS `precio`,`registro_atencion`.`codigo_regi` AS `codigo_regi`,`registro_has_medicamento`.`medicamento_codigo_med` AS `id_med`,`medicamento`.`nombre` AS `nombre_med`,`registro_has_medicamento`.`dosis` AS `dosis` from (((((((`mascota` join `raza` on((`raza`.`idraza` = `mascota`.`raza_idraza`))) join `registro_atencion` on((`mascota`.`codigo_masc` = `registro_atencion`.`mascota_codigo_masc`))) join `registro_has_servicio` on((`registro_atencion`.`codigo_regi` = `registro_has_servicio`.`registro_codigo_regi`))) join `trabajador` on((`registro_has_servicio`.`trabajador_rut` = `trabajador`.`rut`))) join `servicio` on((`registro_has_servicio`.`servicio_codigo_ser` = `servicio`.`codigo_ser`))) join `registro_has_medicamento` on((`registro_atencion`.`codigo_regi` = `registro_has_medicamento`.`registro_codigo_regi`))) join `medicamento` on((`registro_has_medicamento`.`medicamento_codigo_med` = `medicamento`.`codigo_med`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-28 12:49:35
