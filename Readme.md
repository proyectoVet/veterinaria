											
		ATENCION DE UNA VETERINARIA	

La creación de este proyecto esta enfocada en satisfacer una problemática encontrada, la cual consiste en realizar la planificación de una veterinaria.

    • EMPEZANDO

Este programa dispone de una ventana principal que tiene acceso para trabajadores de la veterinaria y para clientes. Al abrir la ventana de trabajadores nos enviará a una pequeña que nos indicará que acción se pueden realizar en la base de datos, ya sea ingresar, editar o eliminar trabajadores (veterinarios), pacientes (mascotas), razas (de las mascotas), clientes (dueños de las mascotas), servicios (que realiza la veterinaria para sus pacientes), medicamentos (recetados para los pacientes), laboratorios (los cuales pertenecen a los medicamentos) y lo último y más importante, el registro de atenciones. En todas estas ventanas se pueden agregar, editar y eliminar datos. 
Para la atencion al cliente se encontrará una ventana que pedirá el código de la mascota para poder ingresar y buscará los datos de esta para poder entregarle al cliente toda la información acerca de las atenciones que tenga en la veterinaria. 

--ESPECIFICACIONES PARA USO DE CODIGO Y PROGRAMA 

Crear base de datos encontrada en dump.
En el archivo database.py se necesita cambiar la contraseña del código a modo personal en la siguiente linea: 
	
	user="root", passwd="CONTRASEÑA MYSQL", database="db_vet")

de esta forma se permitirá la entrada a la base de datos que debe extraerse en el paso anterior. 


    • REQUISITOS PREVIOS
      
-Sistema operativo Linux
-MySQL - Workbench 
-Python 3
-Matplotlib
-Glade/Gtk/Gnome

    • INSTALACIÓN
      
- Para poder ejecutar el programa debemos obtener python 3 el cual se instala con el siguiente comando: 
	
	sudo apt-get install python3 

-Librería Matplorlib para gráficos: 

	sudo apt-get install matplotlib
	sudo apt-get install python3-matplotlib

- Para el uso de MySQL - Workbench debemos ingresar los comandos: 
	
	sudo apt update
	sudo apt install mysql-server
	sudo mysql_secure_installation

  actualizamos el sistema: 

	$ sudo apt-get update

  instalar el paquete mysql-workbench: 
	
	$ sudo apt-get install mysql-workbench

  Si se nos presentan problemas de depencias, ejecutar el siguiente comando:

	$ sudo apt-get install -f

- Realizamos el código en vim, el cual se instala con el siguiente comando:

	sudo apt-get install vim

- Instalación de glade con el comando: 

	apt install glade

Mientras que para la utilizacion de Gtk en python en nuestro entorno virtual debemos instalar el Metapaquete PyGObject utilizando el siguiente comando: pip install wheel PyGObject.
Luego, para comprobar que la instalacion es correcta podemos hacer uso del siguiente comando: python -c 'import gi'. Si esto no arroja ningun tipo de error, quiere decir que la instalacion ha sido exitosa, de no ser asi se debera repetir la instalacion. 


--EJECUTANDO LAS PRUEBAS POR TERMINAL

Para la entrada al programa en el editor de texto vim, se necesita del comando: vim nombrearchivo.py mientras que para ejecutarlo debemos colocar el comando python3 main.py 


    • CONSTRUIDO CON:
      
- Ubuntu: sistema operativo.

- Python: lenguaje de programación utilizado para el código del programa.

- Vim: editor de texto para escribir el código del programa.

- Glade: herramienta de desarollo visual de interfacez gráficas mediante Gtk/gnome.

- MySQL - Workbench: MySQL Workbench es una herramienta visual de diseño de bases de datos que integra desarrollo de software, administración de bases de datos, diseño de bases de datos, gestión y mantenimiento para el sistema de base de datos MySQL.

- Matplotlib: Matplotlib es una biblioteca para la generación de gráficos a partir de datos contenidos en listas o arrays en el lenguaje de programación Python y su extensión matemática NumPy.


    • VERSIONES
      
- Ubuntu 18.04
- python 3.6.9
- Glade 3.22.1 / Gnome 3.28.2 
- MySQL 8.0.22 / 5.7.32
- Workbench 6.3

    • AUTORES

- Nicole soto - Desarrollo del código, ejecución de proyecto y narración de README
- Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README 


    • EXPRESIONES DE GRATITUD

Ejemplos otorgados por Alejandro Valdés Jiménez:
Desde el repositorio -> https://gitlab.com/gtk3/gtk3-samples.git con link directo para clonar. 


