#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

class Servicios:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_servicios = constructor.get_object("vent_visualizar")
        self.ventana_servicios.show_all()

        self.btn_agregar = constructor.get_object("añadir")
        self.btn_agregar.connect("clicked", self.agregar_servicios)

        self.btn_cerrar = constructor.get_object("cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.btn_editar = constructor.get_object("editar")
        self.btn_editar.connect("clicked", self.editar_servicios)

        self.btn_borrar = constructor.get_object("eliminar")
        self.btn_borrar.connect("clicked", self.eliminar_servicios)

        self.treeview_servicios = constructor.get_object("lista")

        self.set_columnas_servicios()

        self.get_servicios()

    def destruir_ventana(self, *args):
        self.ventana_servicios.hide()

    def set_columnas_servicios(self):
        for col in self.treeview_servicios.get_columns():
            self.treeview_servicios.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview_servicios.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_servicios.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Precio", renderer, text=2)
        self.treeview_servicios.append_column(column)

        self.store = Gtk.ListStore(int, str, int) 
        self.treeview_servicios.set_model(self.store)

    def get_servicios(self):
        db = Database()

        sql = """
            select codigo_ser, nombre, precio from servicio
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2]])

    def agregar_servicios(self, button):
        agregar = Nuevo_servicio(True, self);

    def editar_servicios(self, button):
        editar = Nuevo_servicio(False, self);

    def eliminar_servicios(self, button):
        seleccion = self.treeview_servicios.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_servicios, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar(model[treeiter][0])
                self.get_servicios()

            dialog.destroy()

    def eliminar(self, id_servicios):
        db = Database()
        sql = """
            delete from servicio where codigo_ser = %(codigo_ser)s
            """
        db.run_sql(sql, {'codigo_ser': id_servicios}, 'E')

class Nuevo_servicio:
    def __init__(self, agregar_servicio, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_servicio")

        self.btn_cancelar = constructor.get_object("cancelar_servicio")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.btn_guardar = constructor.get_object("guardar_servicio")

        self.nombre_servicio = constructor.get_object("nombre_servicio")
        self.precio_servicio = constructor.get_object("precio_servicio")

        if agregar_servicio:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_servicio)

        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_servicio)

            seleccion = self.obj_padre.treeview_servicios.get_selection()
            model, treeiter = seleccion.get_selected()
    
            if treeiter is not None:
                self.id_servicio = model[treeiter][0]
                db = Database()
                
                sql = """
                    select nombre, precio from servicio where codigo_ser = %(id_servicio)s
                    """
                
                result = db.run_select_filter(sql, {'id_servicio': self.id_servicio})
                self.nombre_servicio.set_text(str(result[0][0]))
                self.precio_servicio.set_text(str(result[0][1]))
                
                self.ventana_agregar.show_all()

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def guardar_servicio(self, button):
        nombre = self.nombre_servicio.get_text()
        precio = self.precio_servicio.get_text()
        
        db = Database()
        
        sql = """
            insert into servicio (nombre, precio) values (%(nom_ser)s, %(pre_ser)s)
            """
        db.run_sql(sql, {"nom_ser": nombre, "pre_ser": precio}, 'I')
        
        self.obj_padre.get_servicios()
        self.ventana_agregar.hide() 

    def actualizar_servicio(self, button):
        nombre = self.nombre_servicio.get_text()
        precio = self.precio_servicio.get_text()
        
        db = Database()
        
        sql = """
            update servicio set nombre = %(nom_ser)s, precio = %(pre_ser)s  where codigo_ser = %(id_servicio)s
            """
        db.run_sql(sql, {"id_servicio": self.id_servicio, "nom_ser": nombre, "pre_ser": precio}, 'A')
        
        self.obj_padre.get_servicios()
        self.ventana_agregar.hide() 
