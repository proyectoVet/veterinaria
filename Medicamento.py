#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

def valor_combo(combo, posicion):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][posicion]
    return seleccion

def editar_combo(codigo, combo, posicion):
    if codigo:
        index = 0
        for ite in combo.get_model():
            if (ite[posicion] == codigo):
                combo.set_active(index)
                break
            index += 1

    else:
        combo.set_active(0)


class Medicamento:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_medicamento = constructor.get_object("vent_visualizar")
        self.ventana_medicamento.show_all()

        self.btn_agregar = constructor.get_object("añadir")
        self.btn_agregar.connect("clicked", self.agregar_medicamento)

        self.btn_editar = constructor.get_object("editar")
        self.btn_editar.connect("clicked", self.editar_medicamento)

        self.btn_borrar = constructor.get_object("eliminar")
        self.btn_borrar.connect("clicked", self.eliminar_medicamento)

        self.btn_cerrar = constructor.get_object("cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.treeview_medicamentos = constructor.get_object("lista")

        self.set_columnas_medicamentos()

        self.get_medicamentos()

    def set_columnas_medicamentos(self):
        for col in self.treeview_medicamentos.get_columns():
            self.treeview_medicamentos.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Código", renderer, text=0)
        self.treeview_medicamentos.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_medicamentos.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Laboratorio", renderer, text=2)
        self.treeview_medicamentos.append_column(column)

        self.store = Gtk.ListStore(int, str, str) 
        self.treeview_medicamentos.set_model(self.store)

    def get_medicamentos(self):
        db = Database()

        sql = """
            select * from view_med_lab;
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2]])


    def destruir_ventana(self, *args):
        self.ventana_medicamento.hide()

    def agregar_medicamento(self, button):
        agregar = Nuevo_medicamento(True, self);

    def editar_medicamento(self, button):
        editar = Nuevo_medicamento(False, self);

    def eliminar_medicamento(self, button):
        seleccion = self.treeview_medicamentos.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_medicamento, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar(model[treeiter][0])
                self.get_medicamentos()

            dialog.destroy()

    def eliminar(self, id_medicamento):
        db = Database()
        sql = """
            delete from medicamento where codigo_med = %(id_medicamento)s
            """
        db.run_sql(sql, {'id_medicamento': id_medicamento}, 'E')

class Nuevo_medicamento:
    def __init__(self, agregar_medicamento, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_medicamento")
        self.ventana_agregar.show_all()

        self.btn_cerrar = constructor.get_object("cancelar_med")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.combo_lab = constructor.get_object("laboratorio")
        renderer = Gtk.CellRendererText()
        self.combo_lab.pack_start(renderer, False)
        self.combo_lab.add_attribute(renderer, "text", 0)
        
        self.store = Gtk.ListStore(str)
        self.combo_lab.set_model(self.store)
        self.get_laboratorio()

        self.btn_guardar = constructor.get_object("boton_med")

        self.nombre_med = constructor.get_object("nombre_med")

        if agregar_medicamento:
            self.combo_lab.set_active(0)

            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_medicamento)

        else:
            self.ventana_agregar.show_all()

            self.btn_guardar.connect("clicked", self.actualizar_medicamento)

            seleccion = self.obj_padre.treeview_medicamentos.get_selection()
            model, treeiter = seleccion.get_selected()
    
            if treeiter is not None:
                self.id_medicamento = model[treeiter][0]
                db = Database()
                
                sql = """
                    select * from view_med_lab where codigo_med = %(id_medicamento)s;
                    """
                result = db.run_select_filter(sql, {'id_medicamento': self.id_medicamento})
                self.nombre_med.set_text(result[0][1])

                lab = result[0][2]
                editar_combo(lab, self.combo_lab, 0)

                self.ventana_agregar.show_all()

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def get_laboratorio(self):
        db = Database()
        
        sql = """
            select nombre from laboratorio
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append(["Seleccionar"])
            for r in result:
                self.store.append([r[0]])

    def guardar_medicamento(self, button):
        nombre = self.nombre_med.get_text() 
        nombre_lab = valor_combo(self.combo_lab, 0)

        db = Database()
       
        if nombre_lab != "Seleccionar" and nombre != " ":
            obtener_id = """
                select idlaboratorio from laboratorio where nombre = %(nom_lab)s;
                """ 
            result = db.run_select_filter(obtener_id, {"nom_lab": nombre_lab})
        
            for i in result:
                for j in i:
                    id_lab = j
        
            sql = """
                insert into medicamento (nombre, laboratorio_idlaboratorio) values (%(nom_med)s, %(id_lab)s)
                """
            db.run_sql(sql, {"nom_med": nombre, "id_lab": id_lab}, 'I')

            self.obj_padre.get_medicamentos()
            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()

    def actualizar_medicamento(self, button):
        nombre = self.nombre_med.get_text() 
        nombre_lab = valor_combo(self.combo_lab, 0)

        db = Database()
        
        if nombre_lab != "Seleccionar" and nombre != " ":
            obtener_id = """
                select idlaboratorio from laboratorio where nombre = %(nom_lab)s;
                """ 
            result = db.run_select_filter(obtener_id, {"nom_lab": nombre_lab})
        
            for i in result:
                for j in i:
                    id_lab = j

            sql = """
                update medicamento set codigo_med = %(id)s, nombre = %(nom)s, laboratorio_idlaboratorio = %(id_lab)s  where codigo_med = %(id)s
                """
            db.run_sql(sql, {"id": self.id_medicamento, "nom": nombre, "id_lab": id_lab}, 'A')
        
            self.obj_padre.get_medicamentos()
            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()
