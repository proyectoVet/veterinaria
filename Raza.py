#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database 
from utils import Mensage

class Raza:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")

        self.ventana_raza = constructor.get_object("vent_visualizar")
        self.ventana_raza.show_all()

        self.btn_agregar = constructor.get_object("añadir")
        self.btn_agregar.connect("clicked", self.agregar_razas)

        self.btn_cerrar = constructor.get_object("cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.btn_editar = constructor.get_object("editar")
        self.btn_editar.connect("clicked", self.editar_razas)

        self.btn_borrar = constructor.get_object("eliminar")
        self.btn_borrar.connect("clicked", self.eliminar_razas)

        self.treeview_razas = constructor.get_object("lista")

        self.set_columnas_razas()

        self.get_razas()


    def destruir_ventana(self, *args):
        self.ventana_raza.hide()

    def set_columnas_razas(self):
        for col in self.treeview_razas.get_columns():
            self.treeview_razas.remove_column(col)
        
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Código", renderer, text=0)
        self.treeview_razas.append_column(column)


        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_razas.append_column(column)

        self.store = Gtk.ListStore(int, str) 
        self.treeview_razas.set_model(self.store)

    def get_razas(self):
        db = Database()

        sql = """
            select idraza, nombre from raza
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1]])

    def agregar_razas(self, button):
        agregar = Nueva_raza(True, self);

    def editar_razas(self, button):
        editar = Nueva_raza(False, self);

    def eliminar_razas(self, button):
        seleccion = self.treeview_razas.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_raza, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar(model[treeiter][0])
                self.get_razas()

            dialog.destroy()

    def eliminar(self, id_razas):
        db = Database()
        sql = """
            delete from raza where idraza = %(id_razas)s
            """
        db.run_sql(sql, {'id_razas': id_razas}, 'E')


class Nueva_raza:
    def __init__(self, agregar_raza, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_agregar")
        self.ventana_agregar.set_title("Registro raza")
     
        self.btn_cancelar = constructor.get_object("cancelar_agregar")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.btn_guardar = constructor.get_object("guardar_agregar")

        self.nombre_raza = constructor.get_object("nombre_agregar")

        if agregar_raza:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_raza)

        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_raza)

            seleccion = self.obj_padre.treeview_razas.get_selection()
            model, treeiter = seleccion.get_selected()
    
            if treeiter is not None:
                self.id_razas = model[treeiter][0]
                db = Database()
                
                sql = """
                    select nombre from raza where idraza = %(id_razas)s
                    """
                
                result = db.run_select_filter(sql, {'id_razas': self.id_razas})
                self.nombre_raza.set_text(str(result[0][0]))
                
                self.ventana_agregar.show_all()

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def guardar_raza(self, button):
        nombre = self.nombre_raza.get_text()
      
        if nombre != "":  
            db = Database()
        
            sql = """
                insert into raza (nombre) values (%(nom_raza)s)
                """
            db.run_sql(sql, {"nom_raza": nombre}, 'I')
        
            self.obj_padre.get_razas()
            self.ventana_agregar.hide() 

        elif nombre == "":
            msg = Mensage(Gtk.MessageType.ERROR, "Error: No se puede realizar la inserción")

    def actualizar_raza(self, button):
        nombre = self.nombre_raza.get_text()
        db = Database()
        
        sql = """
            update raza set nombre = %(nom_raza)s where idraza = %(id_razas)s
            """
        db.run_sql(sql, {"id_razas": self.id_razas, "nom_raza": nombre}, 'A')
        
        self.obj_padre.get_razas()
        self.ventana_agregar.hide()   
