#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

def valor_combo(combo, posicion):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][posicion]
    return seleccion

def valor_id(result):
    for i in result:
        for j in i:
            codigo = j
    return codigo

def editar_combo(codigo, combo, posicion):
    if codigo:
        index = 0
        for ite in combo.get_model():
            if (ite[posicion] == codigo):
                combo.set_active(index)
                break
            index += 1
    else:
        combo.set_active(0)


class Atencion:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_atencion = constructor.get_object("ver_atenciones")
        self.ventana_atencion.show_all()

        self.btn_agregar = constructor.get_object("añadir1")
        self.btn_agregar.connect("clicked", self.agregar_ficha)

        self.btn_borrar = constructor.get_object("eliminar1")
        self.btn_borrar.connect("clicked", self.eliminar_ficha)

        self.btn_editar = constructor.get_object("editar1")
        self.btn_editar.connect("clicked", self.editar_ficha)

        self.btn_atender = constructor.get_object("atender")
        self.btn_atender.connect("clicked", self.agregar_atencion)

        self.btn_editar_atender = constructor.get_object("editar_atencion")
        self.btn_editar_atender.connect("clicked", self.editar_atencion)

        self.btn_borrar_atender = constructor.get_object("eliminar_atencion")
        self.btn_borrar_atender.connect("clicked", self.eliminar_atencion)

        self.btn_recetar = constructor.get_object("recetar")
        self.btn_recetar.connect("clicked", self.agregar_receta)

        self.btn_editar_receta = constructor.get_object("editar_receta")
        self.btn_editar_receta.connect("clicked", self.editar_receta)

        self.btn_borrar_receta = constructor.get_object("eliminar_receta")
        self.btn_borrar_receta.connect("clicked", self.eliminar_receta)

        self.btn_cerrar = constructor.get_object("cerrar1")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.treeview_fichas = constructor.get_object("lista1")
        self.set_columnas_fichas()
        self.get_fichas()

        self.treeview_atenciones = constructor.get_object("lista2")
        self.set_columnas_atenciones()
        self.get_atenciones()

        self.treeview_medicamentos = constructor.get_object("lista3")
        self.set_columnas_medicamentos()
        self.get_medicamentos()

    def set_columnas_fichas(self):
        for col in self.treeview_fichas.get_columns():
            self.treeview_fichas.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Código ficha", renderer, text=0)
        self.treeview_fichas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Paciente", renderer, text=1)
        self.treeview_fichas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha ingreso", renderer, text=2)
        self.treeview_fichas.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Estado atención", renderer, text=3)
        self.treeview_fichas.append_column(column)

        self.store_fichas = Gtk.ListStore(str, str, str, str) 
        self.treeview_fichas.set_model(self.store_fichas)

    def get_fichas(self):
        db = Database()

        sql = """
            select codigo_regi, nombre_masc, fecha_ingreso, estado_atencion from view_estado;
            """ 
        result = db.run_select(sql)
        
        self.store_fichas.clear()
        
        if result:
            for r in result:
                self.store_fichas.append([str(r[0]), r[1], str(r[2]), r[3]])

    def set_columnas_atenciones(self):
        for col in self.treeview_atenciones.get_columns():
            self.treeview_atenciones.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Ficha", renderer, text=0)
        self.treeview_atenciones.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Paciente", renderer, text=1)
        self.treeview_atenciones.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha ingreso", renderer, text=2)
        self.treeview_atenciones.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha retiro", renderer, text=3)
        self.treeview_atenciones.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Veterinario", renderer, text=4)
        self.treeview_atenciones.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Servicio", renderer, text=5)
        self.treeview_atenciones.append_column(column)

        self.store_atenciones = Gtk.ListStore(str, str, str, str, str, str) 
        self.treeview_atenciones.set_model(self.store_atenciones)

    def get_atenciones(self):
        db = Database()

        sql = """
            select registro_codigo_regi, nom_masc, fecha_ingreso, fecha_retiro, nom_tra, nom_ser from view_registro;
            """ 
        result = db.run_select(sql)
        
        self.store_atenciones.clear()
        
        if result:
            for r in result:
                self.store_atenciones.append([str(r[0]), r[1], str(r[2]), str(r[3]), r[4], r[5]])

    def set_columnas_medicamentos(self):
        for col in self.treeview_medicamentos.get_columns():
            self.treeview_atenciones.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Ficha", renderer, text=0)
        self.treeview_medicamentos.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Medicamento", renderer, text=1)
        self.treeview_medicamentos.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Dosis", renderer, text=2)
        self.treeview_medicamentos.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha ingreso", renderer, text=3)
        self.treeview_medicamentos.append_column(column)

        self.store_med = Gtk.ListStore(str, str, str, str) 
        self.treeview_medicamentos.set_model(self.store_med)

    def valor_seccionado(self, treeview_seleccion):
        db = Database()

        obtener_id = """
            select codigo_regi from view_registro_med where nom_masc = %(nombre)s && fecha_ingreso = %(ingreso)s;
            """ 

        model, pathlist = treeview_seleccion.get_selected_rows()
        for path in pathlist:
            treeiter = model.get_iter(path)

            id_ficha = model.get_value(treeiter, 0)
            nombre = model.get_value(treeiter, 1)
            ingreso = model.get_value(treeiter, 2)

            result1 = db.run_select_filter(obtener_id, {"nombre": nombre, "ingreso": ingreso})

            if result1 != []:
                sql = """
                select codigo_regi, nombre_med, dosis, fecha_ingreso from view_registro_med where codigo_regi = %(id)s;
                """ 
        
                result = db.run_select_filter(sql, {"id": id_ficha})
                self.store_med.clear()
        
                if result:
                    for r in result:
                        self.store_med.append([str(r[0]), r[1], r[2], str(r[3])])
            else:
                self.store_med.clear()
    
    def get_medicamentos(self):
        tree_seleccion = self.treeview_atenciones.get_selection()
        tree_seleccion.connect("changed", self.valor_seccionado)

    def destruir_ventana(self, *args):
        self.ventana_atencion.hide()

    def agregar_ficha(self, button):
        agregar = Nueva_ficha(True, self);

    def editar_ficha(self, button):
        editar = Nueva_ficha(False, self);

    def eliminar_ficha(self, button):
        seleccion = self.treeview_fichas.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_atencion, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar1(model[treeiter][0])
                self.get_fichas()
                self.get_atenciones()

            dialog.destroy()

    def eliminar1(self, id_ficha):
        db = Database()
        sql = """
            delete from registro_atencion where codigo_regi = %(id_ficha)s
            """
        db.run_sql(sql, {'id_ficha': id_ficha}, 'E')


    def agregar_atencion(self, button):
        agregar = Nueva_atencion(True, self);

    def editar_atencion(self, button):
        editar = Nueva_atencion(False, self);

    def eliminar_atencion(self, button):
        seleccion = self.treeview_atenciones.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_atencion, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar2(model[treeiter][0])
                self.get_atenciones()
                self.get_fichas()

            dialog.destroy()

    def eliminar2(self, id_ficha):
        db = Database()
        sql = """
            delete from registro_has_servicio where registro_codigo_regi = %(id_ficha)s
            """
        db.run_sql(sql, {'id_ficha': id_ficha}, 'E')
    def agregar_receta(self, button):
        agregar = Nueva_receta(True, self);

    def editar_receta(self, button):
        editar = Nueva_receta(False, self);

    def eliminar_receta(self, button):
        seleccion = self.treeview_medicamentos.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_atencion, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar3(model[treeiter][0])
                self.get_fichas()
                self.get_atenciones()

            dialog.destroy()

    def eliminar3(self, id_ficha):
        db = Database()
        sql = """
            delete from registro_has_medicamento where registro_codigo_regi = %(id_ficha)s
            """
        db.run_sql(sql, {'id_ficha': id_ficha}, 'E')


class Nueva_ficha:
    def __init__(self, agregar_ficha, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_ficha")

        self.btn_guardar = constructor.get_object("registro_guardar")

        self.btn_cancelar = constructor.get_object("registro_cancelar")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.fecha_ingreso = constructor.get_object("ingreso")
        self.fecha_retiro = constructor.get_object("retiro")
        self.codigo_registro = constructor.get_object("codigo")

        # Combo paciente
        self.combo_paciente = constructor.get_object("mascota")

        renderer = Gtk.CellRendererText()
        self.combo_paciente.pack_start(renderer, False)
        self.combo_paciente.add_attribute(renderer, "text", 0)

        renderer = Gtk.CellRendererText()
        self.combo_paciente.pack_start(renderer, False)
        self.combo_paciente.add_attribute(renderer, "text", 1)

        self.store = Gtk.ListStore(str, str)
        self.combo_paciente.set_model(self.store)
        self.get_paciente()

        if agregar_ficha:
            self.combo_paciente.set_active(0)

            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_ficha)
        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_ficha)

            seleccion = self.obj_padre.treeview_fichas.get_selection()
            model, treeiter = seleccion.get_selected()

            if treeiter is not None:
                self.id_ficha = model[treeiter][0]
                db = Database()
                
                sql = """
                    select codigo_regi, nombre_masc, fecha_ingreso, fecha_retiro, estado_atencion from view_estado where codigo_regi = %(id_ficha)s;
                    """
                
                result = db.run_select_filter(sql, {'id_ficha': self.id_ficha})

                self.fecha_ingreso.set_text(str(result[0][2]))
                self.fecha_retiro.set_text(str(result[0][3]))
                paciente = result[0][1]
                editar_combo(paciente, self.combo_paciente, 0)            
    
    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def get_paciente(self):
        db = Database()
        
        sql = """
            select nombre, codigo_masc from mascota
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append(["Seleccionar", " "])
            for r in result:
                self.store.append([r[0], str(r[1])])

    def guardar_ficha(self, button):
        ingreso = self.fecha_ingreso.get_text()
        retiro = self.fecha_retiro.get_text()
        codigo_masc = valor_combo(self.combo_paciente, 1)
        
        db = Database()
        
        sql = """
            insert into registro_atencion (fecha_ingreso, fecha_retiro, mascota_codigo_masc) 
            values (%(ingreso)s, %(retiro)s, %(id_masc)s)
            """
        db.run_sql(sql, {"ingreso": ingreso, "retiro": retiro, "id_masc": codigo_masc}, 'I')

        self.obj_padre.get_fichas()
        self.ventana_agregar.hide() 

    def actualizar_ficha(self, button):
        ingreso = self.fecha_ingreso.get_text()
        retiro = self.fecha_retiro.get_text()
        id_paciente = valor_combo(self.combo_paciente, 1)

        db = Database()

        sql = """
            update registro_atencion set fecha_ingreso = %(ingreso)s, fecha_retiro = %(retiro)s, 
            mascota_codigo_masc = %(mascota)s  where codigo_regi = %(id_ficha)s
            """
        db.run_sql(sql, {"ingreso": ingreso, "retiro": retiro, "mascota": id_paciente, "id_ficha": self.id_ficha}, 'A')
        
        self.obj_padre.get_fichas()
        self.obj_padre.get_atenciones()
        self.obj_padre.get_medicamentos()

        self.ventana_agregar.hide() 

class Nueva_atencion:
    def __init__(self, agregar_atencion, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_atender")

        self.btn_guardar = constructor.get_object("guardar_atender")

        self.btn_cancelar = constructor.get_object("cancelar_atender")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        # Combo servicio
        self.combo_servicio = constructor.get_object("servicio")

        renderer = Gtk.CellRendererText()
        self.combo_servicio.pack_start(renderer, False)
        self.combo_servicio.add_attribute(renderer, "text", 0)

        self.store = Gtk.ListStore(str)
        self.combo_servicio.set_model(self.store)
        self.get_servicio()

        # Combo veterinario
        self.combo_veterinario = constructor.get_object("veterinario")

        renderer = Gtk.CellRendererText()
        self.combo_veterinario.pack_start(renderer, False)
        self.combo_veterinario.add_attribute(renderer, "text", 0)

        renderer = Gtk.CellRendererText()
        self.combo_veterinario.pack_start(renderer, False)
        self.combo_veterinario.add_attribute(renderer, "text", 1)

        self.store = Gtk.ListStore(str, str)
        self.combo_veterinario.set_model(self.store)
        self.get_veterinario()

        if agregar_atencion:
            self.combo_servicio.set_active(0)
            self.combo_veterinario.set_active(0)

            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_atencion)

        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_atencion)

            seleccion = self.obj_padre.treeview_atenciones.get_selection()
            model, treeiter = seleccion.get_selected()

            if treeiter is not None:
                self.id_ficha = model[treeiter][0]
                db = Database()
                
                sql = """
                    select nom_tra, nom_ser from view_registro where registro_codigo_regi = %(id_ficha)s;
                    """
                
                result = db.run_select_filter(sql, {'id_ficha': self.id_ficha})

                veterinario = result[0][0]
                editar_combo(veterinario, self.combo_veterinario, 0)            

                servicio = result[0][1]
                editar_combo(servicio, self.combo_servicio, 0)
            
    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def get_servicio(self):
        db = Database()
        
        sql = """
            select nombre from servicio
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append(["Seleccionar"])
            for r in result:
                self.store.append([r[0]])

    def get_veterinario(self):
        db = Database()
        
        sql = """
            select nombre, rut from trabajador
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append(["Seleccionar", " "])
            for r in result:
                self.store.append([r[0], str(r[1])])

    def guardar_atencion(self, button):
        nom_ser = valor_combo(self.combo_servicio, 0)
        rut = valor_combo(self.combo_veterinario, 1)
        
        # Obtener id de la ficha
        seleccion = self.obj_padre.treeview_fichas.get_selection()
        model, treeiter = seleccion.get_selected()

        if treeiter is not None:
            self.id_ficha = model[treeiter][0]

        db = Database()
        if nom_ser != "Seleccionar" and rut != "Seleccionar":
            # Obtener id del servicio
            obtener_id_ser = """
                select codigo_ser from servicio where nombre = %(nom_ser)s;
                """ 
            result = db.run_select_filter(obtener_id_ser, {"nom_ser": nom_ser})
            id_ser = valor_id(result)
        
            # Agregar atención a la base de datos
            sql = """
                insert into registro_has_servicio (registro_codigo_regi, servicio_codigo_ser, trabajador_rut) 
                values (%(id_regi)s, %(id_ser)s, %(rut)s);
                """
            db.run_sql(sql, {"id_regi": self.id_ficha, "id_ser": id_ser, "rut": rut}, 'I')

            self.obj_padre.get_fichas()
            self.obj_padre.get_atenciones()

            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()

    def actualizar_atencion(self, button):
        veterinario = valor_combo(self.combo_veterinario, 1)
        servicio = valor_combo(self.combo_servicio, 0)

        db = Database()

        if veterinario != "Seleccionar" and servicio != "Seleccionar":
            # Obtener id del servicio
            obtener_id = """
            select codigo_ser from servicio where nombre = %(nombre)s;
            """ 
            result = db.run_select_filter(obtener_id, {"nombre": servicio})
            id_ser = valor_id(result)

            sql = """
                update registro_has_servicio set servicio_codigo_ser = %(ser)s, trabajador_rut = %(rut)s 
                where registro_codigo_regi = %(id)s
                """
            db.run_sql(sql, {"ser": id_ser, "rut": veterinario, "id": self.id_ficha}, 'A')
        
            self.obj_padre.get_atenciones()

            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()

class Nueva_receta:
    def __init__(self, agregar_receta, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_recetar")

        self.btn_guardar = constructor.get_object("guardar_receta")

        self.btn_cancelar = constructor.get_object("cancelar_receta")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.dosis = constructor.get_object("dosis")

        # Combo medicamento
        self.combo_med = constructor.get_object("nom_medi")

        renderer = Gtk.CellRendererText()
        self.combo_med.pack_start(renderer, False)
        self.combo_med.add_attribute(renderer, "text", 0)

        self.store = Gtk.ListStore(str)
        self.combo_med.set_model(self.store)
        self.get_medicamento()

        if agregar_receta:
            self.combo_med.set_active(0)

            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_receta)

        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_receta)

            seleccion = self.obj_padre.treeview_medicamentos.get_selection()
            model, treeiter = seleccion.get_selected()

            if treeiter is not None:
                self.id_ficha = model[treeiter][0]
                db = Database()
                
                sql = """
                    select nombre_med, dosis from view_registro_med where codigo_regi = %(id_ficha)s;
                    """
                
                result = db.run_select_filter(sql, {'id_ficha': self.id_ficha})

                med = result[0][0]
                editar_combo(med, self.combo_med, 0)            

                self.dosis.set_text(result[0][1])

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def get_medicamento(self):
        db = Database()
        
        sql = """
            select nombre from medicamento
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            self.store.append(["Seleccionar"])
            for r in result:
                self.store.append([r[0]])

    def guardar_receta(self, button):
        dosis = self.dosis.get_text()
        nombre_med = valor_combo(self.combo_med, 0)
        
        db = Database()
        
        # Obtener nombre de la mascota y fecha de ingreso
        seleccion = self.obj_padre.treeview_atenciones.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            nombre_paciente = model[treeiter][1]
            ingreso = model[treeiter][2]

        if dosis != "" and nombre_med != "seleccionar": 
            # Obtener id de la mascota
            obtener_id_masc = """
                select codigo_masc from mascota where nombre = %(nombre_paciente)s;
            """
            result = db.run_select_filter(obtener_id_masc, {'nombre_paciente': nombre_paciente})
            codigo_masc = valor_id(result)

            # Obtener id del registro de atenciones de la mascota
            obtener_id_ficha = """
                select codigo_regi from registro_atencion where mascota_codigo_masc = %(id_masc)s && fecha_ingreso = %(ingreso)s;
            """
            result = db.run_select_filter(obtener_id_ficha, {'id_masc': codigo_masc, "ingreso": ingreso})
            codigo_regi = valor_id(result)
            
            # Obtener id del medicamento
            obtener_id_med = """
                select codigo_med from medicamento where nombre = %(nom_med)s;
                """ 
            result = db.run_select_filter(obtener_id_med, {"nom_med": nombre_med})
            id_med = valor_id(result)

            # Agregar medicamento a la base de datos
            sql = """
                insert into registro_has_medicamento (registro_codigo_regi, medicamento_codigo_med, dosis) 
                values (%(id_regi)s, %(id_med)s, %(dosis)s)
                """
            db.run_sql(sql, {"id_regi": codigo_regi, "id_med": id_med, "dosis": dosis}, 'I')
            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()

    def actualizar_receta(self, button):
        dosis = self.dosis.get_text()
        nombre_med = valor_combo(self.combo_med, 0)

        db = Database()

        if dosis != "" and nombre_med != "seleccionar": 
            # Obtener id del medicamento
            obtener_id_med = """
                select codigo_med from medicamento where nombre = %(nom_med)s;
                """ 
            result = db.run_select_filter(obtener_id_med, {"nom_med": nombre_med})
            id_med = valor_id(result)

            sql = """
                update registro_has_medicamento set medicamento_codigo_med = %(id_med)s, dosis = %(dosis)s where registro_codigo_regi = %(id_ficha)s
                """
            db.run_sql(sql, {"id_med": id_med, "dosis": dosis, "id_ficha": self.id_ficha}, 'A')
        
            self.obj_padre.get_medicamentos()
            self.ventana_agregar.hide() 
        else:
            dialog = Gtk.MessageDialog(parent=self.ventana_agregar, flags=0, message_type=Gtk.MessageType.ERROR, 
                buttons=Gtk.ButtonsType.OK, text="ERROR")
            dialog.format_secondary_text("No ha completado todos los datos.")
            response = dialog.run()

            if response == Gtk.ResponseType.OK:
                dialog.destroy()
