#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gi
import matplotlib.pyplot as plt

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database
from Trabajadores import Trabajadores
from Mascotas import Mascotas
from Nuevo_cliente import Nuevo_cliente
from Clientes import Clientes
from Raza import Raza
from Servicios import Servicios
from Atenciones import Atencion
from Laboratorio import Laboratorio
from Medicamento import Medicamento

class Ventana_principal:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_principal = constructor.get_object("vent_inicio")
        self.ventana_principal.connect("destroy", self.destruir_ventana)

        self.btn_trab = constructor.get_object("boton_tra")
        self.btn_trab.connect("clicked", self.abrir_ventana_aux)

        self.btn_cli = constructor.get_object("boton_cli")
        self.btn_cli.connect("clicked", self.ver_masc)

        self.btn_estadistica_cli = constructor.get_object("estadistica_cli")
        self.btn_estadistica_cli.connect("clicked", self.estadistica_cliente)

        self.btn_estadistica_lab = constructor.get_object("estadistica_lab")
        self.btn_estadistica_lab.connect("clicked", self.estadistica_laboratorio)

        self.btn_salir = constructor.get_object("salir_inicio")
        self.btn_salir.connect("clicked", self.destruir_ventana)
        
        self.ventana_principal.show_all()
        Gtk.main()

    def estadistica_cliente(self, btn=None):
        x = Database()
        names = []
        values = [] 
        
        sql = """ select rut from cliente
            """
        nombres = x.run_select(sql)
        for i in nombres:
            sql = """ select count(nombre) from mascota where cliente_rut = %(rut)s
                """
            numero_mascota = x.run_select_filter(sql, {'rut': i[0]})
            values.append(numero_mascota[0][0])

            sql = """ select cliente.nombre from cliente inner join mascota on cliente.rut = mascota.cliente_rut where cliente_rut = %(rut)s
                 """
            nombre_cliente = x.run_select_filter(sql, {'rut': i[0]})
            if nombre_cliente:
                names.append(nombre_cliente[0][0])

        plt.bar(names, values)            
        plt.ylabel("Número de mascota")
        plt.xlabel("Cliente")
        plt.title("Gráfico cliente-mascota")
        plt.show()  

    def estadistica_laboratorio(self, btn=None):
        #   El gráfico para laboratorio y medicamento exige verificar que no existan dos laboratorios con mismo nombre. 
        x = Database()
        names = []
        values = [] 
        
        sql = """ select nombre from laboratorio
            """
        nombres_lab = x.run_select(sql)
        for i in nombres_lab:
            sql = """ select count(medicamento.nombre) from medicamento inner join laboratorio on 
                laboratorio.idlaboratorio = medicamento.laboratorio_idlaboratorio where laboratorio.nombre = %(nom)s
                """
            numero_medicamento = x.run_select_filter(sql, {'nom': i[0]})
            values.append(numero_medicamento[0][0])
            
            names.append(i[0])
            
        plt.bar(names, values)            
        plt.ylabel("Cantidad de medicamentos")
        plt.xlabel("Laboratorio")
        plt.title("Gráfico laboratorio-medicamentos")
        plt.show()

    def destruir_ventana(self, *args):
        Gtk.main_quit()

    def abrir_ventana_aux(self, button):
        auxiliar = Ventana_aux()

    def ver_masc(self, button):
        cliente = Clientes()

class Ventana_aux:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_aux = constructor.get_object("vent_aux")

        self.btn_cerrar = constructor.get_object("boton_cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.btn_ir = constructor.get_object("boton_ir")
        self.btn_ir.connect("clicked", self.registro)

        self.combo = constructor.get_object("combo_aux")
        self.combo.connect("changed", self.valor_combo)

        self.combo.set_active(0)
        self.ventana_aux.show_all()
    
    def destruir_ventana(self, *args):
        self.ventana_aux.hide()

    def valor_combo(self, combo):
        aux = self.combo.get_active_iter()
        if (aux is not None):
            model = self.combo.get_model()
            self.seleccionado = model[aux][0]
            
    def registro(self, combo):
        self.ventana_aux.hide()

        if(self.seleccionado == "Registro trabajadores"):
            trabajador = Trabajadores()
        
        elif(self.seleccionado == "Registro pacientes"):
            mascota = Mascotas()

        elif(self.seleccionado == "Registro clientes"):
            nuevo_cliente = Nuevo_cliente()

        elif(self.seleccionado == "Registro razas"):
            nueva_raza = Raza()

        elif(self.seleccionado == "Registro servicios"):
            nuevo_servicio = Servicios()

        elif(self.seleccionado == "Registro medicamentos"):
            nuevo_medicamento = Medicamento()

        elif(self.seleccionado == "Registro laboratorios"):
            nuevo_lab = Laboratorio()

        elif(self.seleccionado == "Registro atenciones"):
            nueva_atencion = Atencion()

def main():
    db = Database()
    app = Ventana_principal()
    
    return 0


if __name__ == '__main__':
    main()
