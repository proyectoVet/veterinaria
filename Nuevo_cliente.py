#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

class Nuevo_cliente:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")

        self.ventana_cliente = constructor.get_object("vent_visualizar")
        self.ventana_cliente.show_all()

        self.btn_agregar = constructor.get_object("añadir")
        self.btn_agregar.connect("clicked", self.agregar_nuevo_cliente)

        self.btn_editar = constructor.get_object("editar")
        self.btn_editar.connect("clicked", self.editar_cliente)

        self.btn_cerrar = constructor.get_object("cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.btn_borrar = constructor.get_object("eliminar")
        self.btn_borrar.connect("clicked", self.eliminar_cliente)

        self.treeview_clientes = constructor.get_object("lista")

        self.set_columnas_clientes()

        self.get_clientes()

    def destruir_ventana(self, *args):
        self.ventana_cliente.hide()

    def set_columnas_clientes(self):
        for col in self.treeview_clientes.get_columns():
            self.treeview_clientes.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Rut", renderer, text=0)
        self.treeview_clientes.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_clientes.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Teléfono", renderer, text=2)
        self.treeview_clientes.append_column(column)

        self.store = Gtk.ListStore(int, str, int) 
        self.treeview_clientes.set_model(self.store)

    def get_clientes(self):
        db = Database()

        sql = """
            select rut, nombre, num_telefono from cliente
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2]])

    def agregar_nuevo_cliente(self, button):
        agregar = Agregar_cliente(True, self);

    def editar_cliente(self, button):
        editar = Agregar_cliente(False, self);

    def eliminar_cliente(self, button):
        seleccion = self.treeview_clientes.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_cliente, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar(model[treeiter][0])
                self.get_clientes()

            dialog.destroy()

    def eliminar(self, id_cliente):
        db = Database()
        sql = """
            delete from cliente where rut = %(rut_cliente)s
            """
        db.run_sql(sql, {'rut_cliente': id_cliente}, 'E')


class Agregar_cliente:
    def __init__(self, agregar_cliente, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_clientes")
        self.ventana_agregar.show_all()

        self.btn_cancelar = constructor.get_object("cancelar_cli")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.btn_guardar = constructor.get_object("guardar_cli")

        self.rut_cliente = constructor.get_object("rut_cli")
        self.nombre_cliente = constructor.get_object("nombre_cli")
        self.num_cliente = constructor.get_object("num_cli")

        if agregar_cliente:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_cliente)

        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_cliente)

            seleccion = self.obj_padre.treeview_clientes.get_selection()
            model, treeiter = seleccion.get_selected()
    
            if treeiter is not None:
                self.id_cliente = model[treeiter][0]
                db = Database()
                
                sql = """
                    select rut, nombre, num_telefono from cliente where rut = %(id_cliente)s
                    """
                
                result = db.run_select_filter(sql, {'id_cliente': self.id_cliente})
                self.rut_cliente.set_text(str(result[0][0]))
                self.nombre_cliente.set_text(str(result[0][1]))
                self.num_cliente.set_text(str(result[0][2]))
                
                self.ventana_agregar.show_all()

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def guardar_cliente(self, button):
        rut = self.rut_cliente.get_text()
        nombre = self.nombre_cliente.get_text()
        contacto = self.num_cliente.get_text()
        
        db = Database()
        
        sql = """
            insert into cliente (rut, nombre, num_telefono) values (%(rut_cli)s, %(nom_cli)s, %(contacto_cli)s)
            """
        db.run_sql(sql, {"rut_cli": rut, "nom_cli": nombre, "contacto_cli": contacto}, 'I')
        
        self.obj_padre.get_clientes()
        self.ventana_agregar.hide() 

    def actualizar_cliente(self, button):
        rut = self.rut_cliente.get_text()
        nombre = self.nombre_cliente.get_text()
        contacto = self.num_cliente.get_text()
        
        db = Database()
        
        sql = """
            update cliente set rut = %(rut_cli)s, nombre = %(nom_cli)s, num_telefono = %(num)s where rut = %(rut_cli)s
            """
        db.run_sql(sql, {"rut_cli": rut, "nom_cli": nombre, "num": contacto}, 'A')
        
        self.obj_padre.get_clientes()
        self.ventana_agregar.hide() 
