#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database

class Trabajadores:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.ventana_trab = constructor.get_object("vent_visualizar")
        self.ventana_trab.show_all()

        self.btn_agregar = constructor.get_object("añadir")
        self.btn_agregar.connect("clicked", self.agregar_trabajador)

        self.btn_cerrar = constructor.get_object("cerrar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        self.btn_editar = constructor.get_object("editar")
        self.btn_editar.connect("clicked", self.editar_trabajador)

        self.btn_borrar = constructor.get_object("eliminar")
        self.btn_borrar.connect("clicked", self.eliminar_trabajador)

        self.treeview_trabajadores = constructor.get_object("lista")

        self.set_columnas_trabajadores()

        self.get_trabajadores()

    def destruir_ventana(self, *args):
        self.ventana_trab.hide()

    def set_columnas_trabajadores(self):
        for col in self.treeview_trabajadores.get_columns():
            self.treeview_trabajadores.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Rut", renderer, text=0)
        self.treeview_trabajadores.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview_trabajadores.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Teléfono", renderer, text=2)
        self.treeview_trabajadores.append_column(column)

        self.store = Gtk.ListStore(int, str, int) 
        self.treeview_trabajadores.set_model(self.store)

    def get_trabajadores(self):
        db = Database()

        sql = """
            select rut, nombre, num_telefono from trabajador
            """ 
        result = db.run_select(sql)
        
        self.store.clear()
        
        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2]])

    def agregar_trabajador(self, button):
        agregar = Nueva_trabajador(True, self);

    def editar_trabajador(self, button):
        editar = Nueva_trabajador(False, self);

    def eliminar_trabajador(self, button):
        seleccion = self.treeview_trabajadores.get_selection()
        model, treeiter = seleccion.get_selected()
        
        if treeiter is not None:
            dialog = Gtk.MessageDialog(parent=self.ventana_trab, flags=0, message_type=Gtk.MessageType.QUESTION, 
                buttons=Gtk.ButtonsType.YES_NO, text="Se eliminara a " + model[treeiter][1])
            dialog.format_secondary_text("Esta acción no se puede revertir.")
            
            response = dialog.run()
            
            if response == Gtk.ResponseType.YES:
                self.eliminar(model[treeiter][0])
                self.get_trabajadores()

            dialog.destroy()

    def eliminar(self, id_trabajador):
        db = Database()
        sql = """
            delete from trabajador where rut = %(rut_trabajador)s
            """
        db.run_sql(sql, {'rut_trabajador': id_trabajador}, 'E')

class Nueva_trabajador:
    def __init__(self, agregar_trabajador, objeto):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")
        
        self.obj_padre = objeto

        self.ventana_agregar = constructor.get_object("vent_trabajador")

        self.btn_cancelar = constructor.get_object("cancelar_trab")
        self.btn_cancelar.connect("clicked", self.destruir_ventana)

        self.btn_guardar = constructor.get_object("guardar_trab")

        self.rut_trabajador = constructor.get_object("rut_trab")
        self.nombre_trabajador = constructor.get_object("nombre_trab")
        self.num_trabajador = constructor.get_object("num_trab")

        if agregar_trabajador:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.guardar_trabajador)

        else:
            self.ventana_agregar.show_all()
            self.btn_guardar.connect("clicked", self.actualizar_trabajador)

            seleccion = self.obj_padre.treeview_trabajadores.get_selection()
            model, treeiter = seleccion.get_selected()
    
            if treeiter is not None:
                self.id_trabajador = model[treeiter][0]
                db = Database()
                
                sql = """
                    select rut, nombre, num_telefono from trabajador where rut = %(id_trabajador)s
                    """
                
                result = db.run_select_filter(sql, {'id_trabajador': self.id_trabajador})
                self.rut_trabajador.set_text(str(result[0][0]))
                self.nombre_trabajador.set_text(str(result[0][1]))
                self.num_trabajador.set_text(str(result[0][2]))
                
                self.ventana_agregar.show_all()

    def destruir_ventana(self, *args):
        self.ventana_agregar.hide()

    def guardar_trabajador(self, button):
        rut = self.rut_trabajador.get_text()
        nombre = self.nombre_trabajador.get_text()
        contacto = self.num_trabajador.get_text()
        
        db = Database()
        
        sql = """
            insert into trabajador (rut, nombre, num_telefono) values (%(rut_tra)s, %(nom_tra)s, %(contacto_tra)s)
            """
        db.run_sql(sql, {"rut_tra": rut, "nom_tra": nombre, "contacto_tra": contacto}, 'I')
        
        # Vuelve a obtener los datos para refrescar la vista en la ventana 
        self.obj_padre.get_trabajadores()
        self.ventana_agregar.hide() 

    def actualizar_trabajador(self, button):
        rut = self.rut_trabajador.get_text()
        nombre = self.nombre_trabajador.get_text()
        contacto = self.num_trabajador.get_text()
        
        db = Database()
        
        sql = """
            update trabajador set rut = %(rut_tra)s, nombre = %(nom_tra)s, num_telefono = %(num)s where rut = %(rut_tra)s
            """
        db.run_sql(sql, {"rut_tra": rut, "nom_tra": nombre, "num": contacto}, 'A')
        
        # Vuelve a obtener los datos para refrescar la vista en la ventana 
        self.obj_padre.get_trabajadores()
        self.ventana_agregar.hide() 
